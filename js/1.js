var nature=[];
var flowers=[];
var animals=[];
var people=[];

var imagenesnature=['imgs/nature/1.jpg','imgs/nature/2.jpg','imgs/nature/3.jpg','imgs/nature/4.jpg',];
var imagenesflowers=['imgs/flowers/1.jpg','imgs/flowers/2.jpg','imgs/flowers/3.jpg','imgs/flowers/4.jpg','imgs/flowers/5.jpg','imgs/flowers/6.jpg','imgs/flowers/7.jpg','imgs/flowers/8.jpg',];
var imagenesanimals=['imgs/animals/1.jpg','imgs/animals/2.jpg','imgs/animals/3.jpg','imgs/animals/4.jpg',];
var imagenespeople=['imgs/people/1.jpg','imgs/people/2.jpg','imgs/people/3.jpg','imgs/people/4.jpg',];

var carousel=document.querySelectorAll(".thumbnails>div");
var menu=document.querySelectorAll("li");
var objeto;
var posicion;
var posicionb;
var objetob;
var c=0;
var x=0;

window.addEventListener("load",()=>{

    imagenesnature.forEach((v,i)=>{
        nature[i]=new Image();nature[i].src=imagenesnature[i];
    })
    imagenesflowers.forEach((v,i)=>{
        flowers[i]=new Image();flowers[i].src=imagenesflowers[i];
    })
    imagenesanimals.forEach((v,i)=>{
        animals[i]=new Image();animals[i].src=imagenesanimals[i];
    })
    imagenespeople.forEach((v,i)=>{
        people[i]=new Image();people[i].src=imagenespeople[i];
    })

    menu.forEach((v,i)=>{
        menu[i].addEventListener("click",(event)=>{

            objetob=event.target;
            posicionb=objetob.getAttribute("data-numero");
           

            if(posicionb!=1 && posicionb!=2 && posicionb!=3 && posicionb!=4){
                album=nature;
                x=0
                c=0
            }
            if(posicionb==1){
                album=nature;
                x=0;
                c=0;
            }
            if(posicionb==2){
                album=flowers;
                x=0;
                c=0;
            }
            if(posicionb==3){
                album=animals;
                x=0;
                c=0;
            }
            if(posicionb==4){
                album=people;
                x=0;
                c=0;
            }

            carousel.forEach((i,x)=>{
                carousel[x].style.backgroundImage="url("+album[(x)].src+")";
            })

            document.querySelector(".fotoCentro").style.backgroundImage="url("+album[(x)].src+")";    
        })
    }) 
    menu[0].click();
})

document.querySelector(".siguiente").addEventListener("click",()=>{
   
    c=c+1;
    if((c)>(album.length-carousel.length)){
            c=album.length-carousel.length;
        }

    carousel.forEach((i,x)=>{
        carousel[x].style.backgroundImage="url("+album[(x+c)].src+")";
    
    })
})

document.querySelector(".anterior").addEventListener("click",()=>{

    c=c-1;
    if((c)<(0)){
            c=0;
        }

    carousel.forEach((i,x)=>{
        carousel[x].style.backgroundImage="url("+album[(x+c)].src+")";
    
    })
})

carousel.forEach((v)=>{v.addEventListener("click",(event)=>{

        objeto=event.target;
        posicion=objeto.getAttribute("data-numero");
        
        document.querySelector(".fotoCentro").style.backgroundImage="url("+album[(posicion-1)+c].src+")"; 
    })
})